#!/bin/bash
# Server setup for Ubuntu 13.10 Saucy
# If using on another version you'll need to change the postgres apt repo

function check_params {
  if ! [ -n "$1" ]; then echo "usage: setup install"; echo "usage: setup deploy <git url> <app_name>"; exit 1; fi
}

function install_apt_packages {
  apt-get update
  apt-get install vim  build-essential subversion python-dev virtualenvwrapper nginx-light git mercurial libpcre3-dev libpcrecpp0 libssl-dev zlib1g-dev libpq-dev nginx  uwsgi-emperor uwsgi-plugin-python -y
  apt-get build-dep python-imaging -y
}

function install_postgis {
  echo "deb http://apt.postgresql.org/pub/repos/apt/ saucy-pgdg main" > /etc/apt/sources.list.d/pgdg.list
  wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
  sudo apt-get update
  sudo apt-get upgrade -y
  sudo apt-get install postgresql-9.3 python-psycopg2 postgis -y
}

function create_webapps_account {
  adduser --gecos "" webapps
  usermod -G sudo webapps

  runuser -l webapps -c 'mkdir /home/webapps/.ssh'
  runuser -l webapps -c 'chmod 700 /home/webapps/.ssh'
  runuser -l webapps -c 'echo "ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA7KjcZd8MHJ8aoAdnmKR1zBISmru2VVFl6AVHQPH7s754qKTf43kp4OCjTuefje1NhpBL69MaoK6Y+Fc7gTZOx4Ofny1Ju2L8wViw6bIKdSoTOlD3NRqp28sf3doqOf1LxSXjhFrU50FEP3WhrXypCy70HbjmzN/ZSy8trAY8zsUUpi8gxRmR6KA8wThB2KgR0522X1a3mZANOjWMOhTfcedJwxcPj1ELlB7jycTEZrRuiJL3cltWy26iv4XdwdGa2gKi1HwRROYFW/fu2R6cNz2v34pI+1g2OeXRwJIe9CMQWxyha0YdSVtXVdCjCOU+21ld9baWm7UzU+8aoucUPQ== atkinsonr@gmail.com" > .ssh/authorized_keys'
  runuser -l webapps -c 'mkdir /home/webapps/apps'
}

function config_nginx{
  curl https://bitbucket.org/pirandigital/setup/raw/HEAD/nginx.conf | sed -e's/{{app_name}}/$3/' > /etc/nginx/sites-enabled/$3.conf
}

function config_uwsgi{
  curl https://bitbucket.org/pirandigital/setup/raw/HEAD/uwsgi-vassel.ini | sed -e's/{{app_name}}/$3/' > /etc/uwsgi-emperor/vassals/$3.conf
}

function checkout_code{
  runuser -l webapps -c 'git clone $2 /home/webapps/apps/$3'
  runuser -l webapps -c 'mkvirtualenv $3'
  runuser -l webapps -c 'workon $3'
  runuser -l webapps -c 'add2virtualenv /home/webapps/apps/$3/project'
  runuser -l webapps -c 'add2virtualenv /home/webapps/apps/$3/lib'
}

if [ "$1" = "install" ]
then
  if ! [ -n "$1" ]; then echo "usage: setup install"; echo "usage: setup deploy <git url> <app_name>"; exit 1; fi
  install_apt_packages
  install_postgis
  create_webapps_account
fi

if [ "$1" = "deploy" ]
then
  if ! [ -n "$2" ]; then echo "usage: setup install"; echo "usage: setup deploy <git url> <app_name>"; exit 1; fi
  if ! [ -n "$3" ]; then echo "usage: setup install"; echo "usage: setup deploy <git url> <app_name>"; exit 1; fi
  config_nginx
  config_uwsgi
  checkout_code
fi

